import { describe, expect, it } from "vitest";
import { getDateFormat } from "./dateCalculate";

describe("Commom Function Test", () => {
  it("get right Date format", () => {
    let date = new Date(2014, 3, 6);

    const dateFormat = getDateFormat(date);
    expect(dateFormat).toBe("7/4/2014");
  });
});
