import { localstorageFieldEnum } from "../enum/cookie-filed.enum";

export const setLocalDarkMode = (isDark: boolean): void => {
  localStorage.setItem(localstorageFieldEnum.Dark, String(isDark));
};

export const getLocalDarkMode = (): boolean => {
  let isDark = localStorage.getItem(localstorageFieldEnum.Dark);
  return isDark ? false : Boolean(isDark);
};
