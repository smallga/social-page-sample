export const nowDateDiffSec = (targetDate: Date) => {
  let nowDate = new Date();
  let diff = nowDate.getTime() - targetDate.getTime();
  return diff / 1000;
};

export const getDateFormat = (targetDate: Date) => {
  return `${targetDate.getDate() + 1}/${
    targetDate.getMonth() + 1
  }/${targetDate.getFullYear()}`;
};
