export interface PostModel {
  id?: string;
  authorName: string;
  authorPhotoUrl: string;
  lastUpdated: string;
  title: string;
  replyCount: number;
  likeCount: number;
  webmVideoUrl?: string;
  videoUrl?: string;
  videoPreviewUrl?: string;
  isLike: boolean;
}

export interface CreatePostModel {
  id?: string;
  authorName: string;
  authorPhotoUrl: string;
  lastUpdated: string;
  title: string;
  replyCount: number;
  likeCount: number;
  webmVideoUrl?: string;
  videoUrl?: string;
  videoPreviewUrl?: string;
}
