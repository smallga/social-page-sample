import CreatePostSlice from "./create-post";
import postSlice from "./post-slice";

const rootReducer = {
  post: postSlice,
  createPost: CreatePostSlice,
};

export default rootReducer;
