import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { CreatePostModel, PostModel } from "../../utility/interface/post-model";

interface CreatePostSliceModel {
  newPost?: CreatePostModel; //新的 Post
  isCreating: boolean;
}

export const CreatePostSlice = createSlice({
  name: "createPost",
  initialState: {
    isCreating: false,
    newPost: undefined,
  } as CreatePostSliceModel,
  reducers: {
    setCreatePost: (
      state: CreatePostSliceModel,
      action: PayloadAction<CreatePostModel>
    ) => {
      state.newPost = action.payload;
    },
    setIsCreating: (
      state: CreatePostSliceModel,
      action: PayloadAction<boolean>
    ) => {
      state.isCreating = action.payload;
    },
    createPost: (state) => {},
  },
});

export const { setCreatePost, setIsCreating, createPost } =
  CreatePostSlice.actions;

export const getCreatePost = (state: RootState) => state.createPost.newPost;
export const getIsCreating = (state: RootState) => state.createPost.isCreating;

export default CreatePostSlice.reducer;
