import {
  put,
  takeLatest,
  all,
  call,
  takeEvery,
  take,
  select,
} from "redux-saga/effects";
import { useAppSelector } from "../../hook/redux-hook";
import { createPostAxios, getPost } from "../../server/post";
import {
  getCreatePost,
  setCreatePost,
  setIsCreating,
  createPost,
} from "../reducers/create-post";
import {
  getPosts,
  getPostsFirst,
  getPreviosPost,
  setNewPost,
  setPerviosPost,
  setPostIsFetchComplete,
  setPostIsFetching,
  setPosts,
} from "../reducers/post-slice";
import { sagaAction } from "./sagaAction";

function* getPreviousPosts(): Generator<any, any, any> {
  yield put(setPostIsFetching());
  const data = yield call(getPost);
  if (data) {
    yield put(setPerviosPost(data.data.data));
  }
  yield put(setPostIsFetchComplete());
}

function* getFirstPosts(): Generator<any, any, any> {
  yield put(setPostIsFetching());
  const data = yield call(getPost);
  yield put(setPostIsFetchComplete());
  yield put(setPosts(data.data.data));
}

function* createPostApi(): Generator<any, any, any> {
  console.log("createPostApi");
  yield put(setIsCreating(true));
  const data = yield call(createPostAxios);
  let createPostObj = yield select(getCreatePost);
  if (createPostObj) {
    yield put(setNewPost({ ...createPostObj, isLike: false }));
  }
  console.log(createPostObj);
  yield put(setIsCreating(false));
}

function* rootSaga() {
  yield all([
    takeEvery(getPreviosPost.type, getPreviousPosts),
    takeLatest(getPostsFirst.type, getFirstPosts),
    takeEvery(createPost.type, createPostApi),
  ]);
}

export default rootSaga;
