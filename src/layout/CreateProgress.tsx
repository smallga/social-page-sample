import { useEffect, useState } from "react";
import Progress from "../components/Progress";
import { useAppSelector } from "../hook/redux-hook";
import { getCreatePost, getIsCreating } from "../store/reducers/create-post";
import { CreatePostModel } from "../utility/interface/post-model";

interface CreateProgressProps {}
export default function CreateProgress(props: CreateProgressProps) {
  const [progress, setProgress] = useState(0);
  const useSelect = useAppSelector;
  const createPost = useSelect(getCreatePost);
  const isCreating = useSelect(getIsCreating);

  return (
    <div className="flex h-full w-full items-center">
      <img
        className="mr-2 h-8 w-8 object-contain"
        src={createPost && createPost.videoPreviewUrl}
      ></img>
      <div className="mx-4 flex-1">
        <Progress duration={5000} done={!isCreating} />
      </div>
    </div>
  );
}
