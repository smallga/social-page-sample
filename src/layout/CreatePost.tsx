import { useCallback, useEffect, useRef, useState } from "react";
import { ReactComponent as NewPost } from "../assets/icon/circle-add.svg";
import { ReactComponent as CameraGrey } from "../assets/icon/camera-grey.svg";
import { useAppDispatch } from "../hook/redux-hook";
import {
  setCreatePost,
  setIsCreating,
  createPost,
} from "../store/reducers/create-post";
import { CreatePostModel } from "../utility/interface/post-model";
import { getDateFormat } from "../utility/common/dateCalculate";
import { sagaAction } from "../store/sagas/sagaAction";

interface CreatePostProps {
  show: boolean;
  handleClose: Function;
}
export default function CreatePost(props: CreatePostProps) {
  const { show } = props;

  const [modalShow, setModalShow] = useState(false);
  const [previewImage, setPreviewImage] = useState<string>("");
  const fileInputRef = useRef<HTMLInputElement>(null);
  const titleTextAreaRef = useRef<HTMLTextAreaElement>(null);
  const dispatch = useAppDispatch();

  const setCloseTimeout = useCallback(() => {
    setTimeout(() => {
      setModalShow(false);
      setPreviewImage("");
    }, 300);
  }, []);

  const closeModal = () => {
    const { handleClose } = props;
    if (handleClose) {
      handleClose();
    }
  };

  useEffect(() => {
    if (show) {
      setModalShow(true);
    } else {
      setCloseTimeout();
    }
  }, [show]);

  const clickFileInput = () => {
    if (fileInputRef && fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const handleFileInputChange = (event: any) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        setPreviewImage(String(reader.result));
      };
    }
  };

  const handlleCreatePost = () => {
    let newPost: CreatePostModel = {
      id: String(new Date().getMilliseconds()),
      authorName: "Mike",
      authorPhotoUrl: "/images/mike.png",
      lastUpdated: getDateFormat(new Date()),
      title:
        titleTextAreaRef && titleTextAreaRef.current
          ? titleTextAreaRef.current.value
          : "",
      replyCount: 0,
      likeCount: 0,
      videoPreviewUrl: previewImage,
    };
    dispatch(setCreatePost(newPost));
    dispatch(createPost());
    closeModal();
  };

  return (
    <>
      {modalShow && (
        <>
          <div
            aria-label="modal-background"
            className="fixed top-0 left-0 z-createPost h-full w-full bg-slate-800 bg-opacity-40 md:py-8"
            onClick={closeModal}
          >
            <div
              aria-label="post-modal-content"
              className={`mx-auto h-[calc(100%-3rem)]  ${
                show
                  ? "lg:animate-pulseIn xs:animate-transformUp"
                  : "lg:scale-[0.25] lg:animate-pulseOut xs:translate-y-full xs:animate-transformDown"
              }`}
            >
              <div
                aria-label="post-modal-content"
                className="mx-auto flex h-full w-full max-w-[568px] flex-col overflow-y-auto bg-white px-4 py-2 dark:bg-neutral-900 sm:rounded-md"
                onClick={(e) => {
                  e.stopPropagation();
                }}
              >
                <h2>發佈貼文</h2>
                <div
                  className="relative mt-2 flex h-[250px] cursor-pointer items-center justify-center rounded-lg bg-slate-200 duration-300 hover:brightness-90"
                  onClick={clickFileInput}
                >
                  <CameraGrey className="h-16 w-16" />
                  <input
                    ref={fileInputRef}
                    type="file"
                    accept="image/jpeg, image/png"
                    className="invisible absolute top-1/2"
                    onChange={handleFileInputChange}
                  />
                  {previewImage && (
                    <div className="absolute top-0 left-0 h-full w-full rounded-md bg-white">
                      <img
                        className="h-full w-full rounded-md object-contain"
                        src={previewImage}
                        alt="preview"
                      />
                    </div>
                  )}
                </div>
                <textarea
                  ref={titleTextAreaRef}
                  className="mt-2 w-full flex-1 text-left dark:bg-neutral-900"
                  placeholder="請輸入貼文內容..."
                ></textarea>
                <button
                  onClick={handlleCreatePost}
                  className="h-12 w-full rounded-md bg-slate-600 text-lg font-bold tracking-widest text-white"
                >
                  發佈貼文
                </button>
              </div>
            </div>
          </div>
          <div className="fixed left-1/2 bottom-1 z-[150] h-10 w-10 -translate-x-1/2 rounded-full bg-white lg:hidden">
            <NewPost
              className={`h-10 w-10 ${
                show
                  ? "rotate-45 animate-ratate45Clockwise"
                  : "rotate-0 animate-ratate45ConterClockwise"
              }`}
              onClick={closeModal}
            />
          </div>
        </>
      )}
    </>
  );
}
