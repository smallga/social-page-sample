import { ReactNode } from "react";

interface PageActionNoticationProps {
  children: ReactNode;
  ref: React.MutableRefObject<null>;
}
export default function PageActionNotication(props: PageActionNoticationProps) {
  return (
    <div
      className="fixed right-0 bottom-12 z-40 h-12 w-full bg-white p-2 lg:right-6 lg:bottom-6 lg:h-16 lg:w-[400px] lg:shadow-lg"
      ref={props.ref}
    >
      {props.children}
    </div>
  );
}
