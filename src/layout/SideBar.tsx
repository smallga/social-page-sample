import { NavLink, useNavigate, useRoutes } from "react-router-dom";
import { ReactComponent as Home } from "../assets/icon/home.svg";
import { ReactComponent as Grid } from "../assets/icon/grid.svg";
import { ReactComponent as NewPost } from "../assets/icon/circle-add.svg";
import { ReactComponent as Bell } from "../assets/icon/bell.svg";
import { ReactComponent as Camera } from "../assets/icon/camera.svg";
import PhotoSticker from "../components/PhotoSticker";
import { useCallback } from "react";

interface SideBarProps {
  handleClickAdd?: Function;
}
export default function SideBar(props: SideBarProps) {
  const navigate = useNavigate();

  const IconBtn = (IconProps: any) => {
    return (
      <div
        className="flex h-full w-full items-center justify-center text-neutral-900 dark:text-neutral-50 lg:h-16"
        {...IconProps}
      >
        {IconProps.children}
      </div>
    );
  };

  const clickAdd = () => {
    const { handleClickAdd } = props;
    if (handleClickAdd) {
      handleClickAdd();
    }
  };

  const goHome = useCallback(() => {
    navigate("");
  }, []);

  return (
    <div className="fixed  bottom-0 left-0 z-40 flex h-12 w-full justify-around bg-white dark:border-neutral-500 dark:bg-neutral-700 lg:top-0 lg:h-full lg:w-24 lg:flex-col lg:justify-start lg:border-r-2 lg:pt-2 xl:w-[120px] ">
      <div className="hidden cursor-pointer lg:block" onClick={goHome}>
        <IconBtn>
          <Camera className="h-8 w-8" />
        </IconBtn>
      </div>
      <NavLink to={""}>
        <IconBtn>
          <Home className="home h-8 w-8" />
          <span className="ml-2 hidden lg:block">首頁</span>
        </IconBtn>
      </NavLink>
      <NavLink to={"/grid"}>
        <IconBtn>
          <Grid className="grid h-8 w-8" />
          <span className="ml-2 hidden lg:block">探索</span>
        </IconBtn>
      </NavLink>
      <a className="z-[150] cursor-pointer lg:hidden">
        <IconBtn onClick={clickAdd}>
          <NewPost className="h-8 w-8 rounded-full bg-white" />
        </IconBtn>
      </a>
      <NavLink to={"/notify"}>
        <IconBtn>
          <Bell className="bell h-8 w-8" />
          <span className="ml-2 hidden lg:block">通知</span>
        </IconBtn>
      </NavLink>
      <NavLink to={"/user"}>
        <IconBtn>
          <PhotoSticker url="./images/mike.png" />
          <span className="ml-2 hidden lg:block">個人</span>
        </IconBtn>
      </NavLink>
    </div>
  );
}
