import { useEffect, useState } from "react";

interface ProgressProps {
  duration: number;
  done: boolean;
}
export default function Progress(props: ProgressProps) {
  const { duration, done } = props;

  return (
    <div className="h-1 w-full rounded-md bg-slate-200">
      <div
        className={`${
          done ? "w-full" : "animate-progressAnimation"
        } duration-[${duration}ms] h-1 rounded-md bg-slate-600`}
      ></div>
    </div>
  );
}
