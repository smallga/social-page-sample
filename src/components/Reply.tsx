import { ReplyModel } from "../utility/interface/reply-model";
import PhotoSticker from "./PhotoSticker";

interface ReplyProps {
  reply: ReplyModel;
}
export default function Reply(props: ReplyProps) {
  const { reply } = props;

  return (
    <div className="flex items-start">
      <PhotoSticker url={reply.replyAccountPhoto}></PhotoSticker>
      <div className="flex-1 pl-2 text-left">
        <div className="mt-1 font-bold text-slate-600 dark:text-slate-300">
          {reply.replyAccountName}
          <span className="ml-2 text-xs text-slate-400 dark:text-slate-200">
            {reply.replyDate}
          </span>
        </div>
        <div
          aria-label="reply-message"
          className="mt-1 text-slate-500 dark:text-slate-200"
        >
          {reply.replyMessage}
        </div>
      </div>
    </div>
  );
}
