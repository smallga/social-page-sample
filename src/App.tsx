import { useCallback, useEffect, useRef, useState } from "react";
import "./App.css";
import Header from "./layout/Header";
import SideBar from "./layout/SideBar";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import UserPage from "./pages/User";
import GridPage from "./pages/Grid";
import NotifyPage from "./pages/Notify";
import CreatePost from "./layout/CreatePost";
import { CSSTransition } from "react-transition-group";
import { useAppSelector } from "./hook/redux-hook";
import { getIsCreating } from "./store/reducers/create-post";
import PageActionNotication from "./layout/PageActionNotication";
import CreateProgress from "./layout/CreateProgress";
import { ReactComponent as NewPost } from "./assets/icon/circle-add.svg";
import { getCookie, setCookie } from "./utility/common/cookie";
import { localstorageFieldEnum } from "./utility/enum/cookie-filed.enum";
import {
  setLocalDarkMode,
  getLocalDarkMode,
} from "./utility/common/localstorage";
import { setDarkTheme } from "./utility/common/theme";

function App() {
  const [showCreatePost, setShowCreatePost] = useState(false);
  const [isDark, setIsDark] = useState(getLocalDarkMode());
  const useSelect = useAppSelector;
  const isCreating = useSelect(getIsCreating);
  const nodeRef = useRef(null);

  const closeCreateModal = useCallback(() => {
    setShowCreatePost(false);
  }, []);

  const openCreateModal = useCallback(() => {
    setShowCreatePost(true);
  }, []);

  const changeDark = useCallback(() => {
    setDarkTheme(!isDark);
    setLocalDarkMode(!isDark);
    setIsDark(!isDark);
  }, [isDark]);

  useEffect(() => {
    const prefersDark = window.matchMedia("(prefers-color-scheme: dark)");

    toggleDarkTheme(prefersDark.matches);

    // Listen for changes to the prefers-color-scheme media query
    prefersDark.addEventListener("change", (mediaQuery) =>
      toggleDarkTheme(mediaQuery.matches)
    );

    // Add or remove the "dark" class based on if the media query matches
    function toggleDarkTheme(shouldAdd: boolean) {
      setDarkTheme(shouldAdd);
      setIsDark(shouldAdd);
    }

    return prefersDark.removeEventListener("change", (mediaQuery) =>
      toggleDarkTheme(mediaQuery.matches)
    );
  }, []);

  useEffect(() => {
    let isDark = getCookie(localstorageFieldEnum.Dark);
    if (isDark) {
      setIsDark(Boolean(isDark));
    }
  }, []);

  return (
    <div className="relative h-full w-full overflow-hidden pb-14 pt-2 lg:pt-14 lg:pb-2">
      <Header darkMode={isDark} changeDarkMode={changeDark}></Header>
      <SideBar handleClickAdd={openCreateModal}></SideBar>
      <div
        id="content"
        className="mx-auto h-full w-full overflow-hidden lg:pl-24"
      >
        <Routes>
          <Route path="/*" element={<Home />} />
          <Route path="/grid/*" element={<GridPage />} />
          <Route path="/notify/*" element={<NotifyPage />} />
          <Route path="/user" element={<UserPage />} />
          <Route path="/user/:userName" element={<UserPage />} />
        </Routes>
      </div>
      <div
        className="fixed right-8 bottom-8 hidden h-12 w-12 cursor-pointer rounded-full bg-neutral-50 shadow-md shadow-slate-600 lg:block"
        onClick={openCreateModal}
      >
        <NewPost className="h-12 w-12"></NewPost>
      </div>
      <CreatePost show={showCreatePost} handleClose={closeCreateModal} />
      <CSSTransition
        in={isCreating}
        unmountOnExit
        timeout={300}
        classNames="notification"
        ref={nodeRef}
      >
        <PageActionNotication ref={nodeRef}>
          <CreateProgress />
        </PageActionNotication>
      </CSSTransition>
    </div>
  );
}

export default App;
